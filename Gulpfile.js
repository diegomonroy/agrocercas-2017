// JavaScript Document

/* ************************************************************************************************************************

Agrocercas

File:			Gulpfile.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

// Requires

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var minifyCSS = require('gulp-minify-css');
var minifyJS = require('gulp-uglify');
var image = require('gulp-imagemin');
var watch = require('gulp-watch');

// CSS

gulp.task('css', function () {
	return gulp
		.src('templates/agrocercas/assets/css/app.scss')
		.pipe(sass())
		.pipe(minifyCSS())
		.pipe(rename('app.css'))
		.pipe(gulp.dest('templates/agrocercas/build'));
});

// JS

gulp.task('js', function () {
	return gulp
		.src('templates/agrocercas/assets/js/app.js')
		.pipe(minifyJS())
		.pipe(rename('app.js'))
		.pipe(gulp.dest('templates/agrocercas/build'));
});

// Image

gulp.task('image', function () {
	return gulp
		.src('templates/agrocercas/assets/images/*')
		.pipe(image())
		.pipe(gulp.dest('templates/agrocercas/build'));
});

// Watch

gulp.task('watch', function () {
	gulp.watch(['templates/agrocercas/assets/css/**/*.scss'], ['css']);
	gulp.watch(['templates/agrocercas/assets/js/**/*.js'], ['js']);
});

// Default

gulp.task('default', ['css', 'js', 'image', 'watch']);