<?php

/* ************************************************************************************************************************

Agrocercas

File:			index.php
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

defined( '_JEXEC' ) or die( 'Acceso Restringido.' );

JHtml::_( 'behavior.framework', true );

// Variables

$site_base = $_SERVER['HTTP_HOST']; // e.g. www.amapolazul.com
$site_path = 'http://' . $site_base; // e.g. http://www.amapolazul.com
$app = JFactory::getApplication();
$option = JRequest::getVar( 'option' );
$view = JRequest::getVar( 'view' );
$Itemid = JRequest::getVar( 'Itemid' );
$pageclass = $app->getParams( 'com_content' );
$class = $pageclass->get( 'pageclass_sfx' );
$url = JURI::current();
$url = explode( '/', $url );

// Template path

$path = 'templates/' . $this->template . '/';

// Modules

$show_banner_home = $this->countModules( 'banner_home' );
$show_home = $this->countModules( 'home' );

// Params

?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="google-site-verification" content="AdYUwddR-oHPtSCKhCzrfHFFur5-VMEuF1vBn0tYJyg">
		<!-- Begin Open Graph Protocol -->
		<meta property="og:url" content="<?php echo $site_path; ?>">
		<meta property="og:type" content="website">
		<meta property="og:title" content="<?php echo $app->getCfg( 'sitename' ); ?>">
		<meta property="og:description" content="<?php echo $app->getCfg( 'MetaDesc' ); ?>">
		<meta property="og:image" content="<?php echo $site_path; ?>/<?php echo $path; ?>build/logo_ogp.png">
		<link rel="image_src" href="<?php echo $site_path; ?>/<?php echo $path; ?>build/logo_link_ogp.png">
		<!-- End Open Graph Protocol -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href="<?php echo $path;?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo $path;?>assets/css/bootstrap.vertical-tabs.min.css">
    <link href="<?php echo $path;?>assets/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $path; ?>build/bower_components/fancybox/dist/jquery.fancybox.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>build/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>build/app.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<?php echo $path;?>assets/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="<?php echo $path;?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo $path;?>assets/js/js.js"></script>
		<script src="<?php echo $path; ?>build/bower_components/fancybox/dist/jquery.fancybox.min.js" type="text/javascript"></script>
		<jdoc:include type="head" />
      <script>

      if (typeof modBootstrapAccordionMenu_hover === "undefined") {
        var modBootstrapAccordionMenu_hover = 1;
      }

      window.addEvent('domready', function(){
        if (typeof jQuery != 'undefined' && typeof MooTools != 'undefined' ) {
          Element.implement({
            slide: function(how, mode){
              return this;
            }
          });
        }
      });

      </script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
    $catid = null;
    $input=Jfactory::getApplication()->input;
    if($input->getCmd('option')=='com_content'
    && $input->getCmd('view')=='article' ) {
      $db=JFactory::getDbo();
      $db->setQuery('select catid from #__content where id='.$input->getInt('id'));
      $catid=$db->loadResult();
    } ?>
		<!-- Begin Google Analytics -->
		<script>

			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-101008686-1', 'auto');
			ga('send', 'pageview');

		</script>
		<!-- End Google Analytics -->
	</head>
  <body>
		<?php if ( $option == 'com_content' || $view == 'featured' || $Itemid == 101 ) : ?>
<div style="display: none;" id="hidden-content">
<video width="400" height="400" autoplay loop controls>
<source src="<?php echo $path;?>assets/video/video.mp4" type="video/mp4">
</video>
</div>
		<a data-fancybox data-src="#hidden-content" href="javascript:;" id="hidden_link"></a>
		<script type="text/javascript">
			$(document).ready(function() {
				$( '#hidden_link' ).trigger( 'click' );
			});
		</script>
		<?php endif; ?>
    <div class="top">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 col-sm-9 col-xs-8">
            <jdoc:include type="modules" name="buscar" style="xhtml" />
          </div>
          <div class="col-lg-2 col-sm-3 col-xs-4">
            <jdoc:include type="modules" name="redes" style="xhtml" />
          </div>
        </div>
      </div>
    </div>

    <header>
      <div class="container">
        <div class="row">
          <div class="col-lg-3">
            <jdoc:include type="modules" name="logo" style="xhtml" />
          </div>
          <div class="col-lg-9">
            <nav>
              <jdoc:include type="modules" name="menu" style="xhtml" />
            </nav>
          </div>
        </div>
      </div>
    </header>

	<?php if ( $show_banner_home ) : ?>
		<section class="banner_home">
			<jdoc:include type="modules" name="banner_home" style="xhtml" />
		</section>
	<?php endif; ?>
	<?php if ( $show_home ) : ?>
		<section class="home">
			<jdoc:include type="modules" name="home" style="xhtml" />
		</section>
	<?php endif; ?>

    <section class="a">
      <div class="container">
        <div class="row">
          <?php if(in_array('proyectos', $url) || $catid == 13): ?>
            <div class="col-lg-3">
              <jdoc:include type="modules" name="menu_proyectos" style="xhtml" />
            </div>
            <div class="col-lg-9">
              <jdoc:include type="modules" name="banner_proyectos" style="xhtml" />
            </div>
        <?php endif; ?>
          <?php if(!in_array('productos', $url)): ?>
              <!-- <div class="col-lg-3">
                <jdoc:include type="modules" name="categorias_producto" style="xhtml" />
              </div> -->
          <div class="col-lg-12">
            <jdoc:include type="modules" name="banner" style="xhtml" />
          </div>
            <?php endif; ?>
            <?php if(in_array('productos', $url)): ?>
              <div class="col-lg-3">
                <jdoc:include type="modules" name="categorias_producto" style="xhtml" />
              </div>
              <div class="col-lg-9">
                <div class="contenido">
                  <jdoc:include type="component" />
                </div>
              </div>
            <?php else: ?>
              <div class="col-lg-12">
                <div class="contenido">
                  <jdoc:include type="component" />
                </div>
              </div>
            <?php endif; ?>


        </div>
      </div>
    </section>


    <section class="b">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <jdoc:include type="modules" name="video" style="xhtml" />
          </div>
          <div class="col-lg-8">
            <jdoc:include type="modules" name="ultimas_noticias" style="xhtml" />
          </div>
        </div>
      </div>
    </section>

    <section class="c">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <jdoc:include type="modules" name="productos_destacados" style="xhtml" />
              <br>
          </div>
          <div class="col-lg-2 text-center">
            <p style="font-size: 1.313rem;"><span style="color: #999999;">DISTRIBUIDORES&nbsp;</span><br><span style="color: #999999;">AUTORIZADOS</span></p>
          </div>
          <div class="col-lg-10">
            <jdoc:include type="modules" name="distribuidores" style="xhtml" />
          </div>
        </div>
      </div>
    </section>

    <footer>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <jdoc:include type="modules" name="info" style="xhtml" />
          </div>
        </div>
      </div>
    </footer>
    <div class="derechos">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <p class="text-center">
              Todos los derechos reservados a Agrocercas / Sitio desarrollado por <a href="http://amapolazul.com" target="_blank">Amapolazul</a>.
            </p>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
